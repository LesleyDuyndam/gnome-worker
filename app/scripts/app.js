'use strict';

var GNOMEWORKER = function () {
    var root = this;
    root.helpers = {};
    window.$GnomeWorker = this;
};
GNOMEWORKER.prototype.hasHelper = function (helperName) {
    var root = this;
    return helperName in root.helpers && typeof root.helpers[helperName] === 'object';
};
GNOMEWORKER.prototype.hasEventHandler = function (helperName) {
    var root = this;
    if(!root.hasHelper(helperName)) {
        return false;
    }
    return typeof root.helpers[helperName].eventHandlers === 'object';
};
GNOMEWORKER.prototype.hasEvent = function (helperName, eventName) {
    var root = this;
    if(!root.hasEventHandler(helperName)) {
        return false;
    }
    return $.isArray(root.helpers[helperName].eventHandlers[eventName]);
};
GNOMEWORKER.prototype.addHelper = function (HelperClass, helperSettings) {
    var root = this;
    if (root.hasHelper(HelperClass.name)) {
        return root.throwError(103, 'Trying to set helper "' + HelperClass.name + '", but is already exists!');
    }
    root.helpers[HelperClass.name] = new HelperClass(helperSettings);
};
GNOMEWORKER.prototype.throwError = function (code, message) {
    console.error('Error code ' + code + '  - - - - - - - -');
    console.error(message);
    console.error('- - - - - - - - - - - - - - - -');
    return false;
};
GNOMEWORKER.prototype.addEventHandler = function (helper, eventId, eventHandler) {
    var root = this;
    if (!root.hasHelper(helper)) {
        return root.throwError(104, 'Helper "' + helper + '" is not registered');
    }
    if (!root.hasEventHandler(helper)) {
        root.helpers[helper].eventHandlers = {};
    }
    if (!root.hasEvent(helper, eventId)) {
        root.helpers[helper].eventHandlers[eventId] = [];
    }
    root.helpers[helper].eventHandlers[eventId].push(eventHandler);
};
GNOMEWORKER.prototype.on = function (helper, eventId, eventHandler) {
    var root = this;
    if (!root.hasHelper(helper)) {
        return root.throwError(105, 'Helper "' + helper + '" is not registered.');
    }
    root.addEventHandler(helper, eventId, eventHandler);

};
GNOMEWORKER.prototype.broadcast = function (helper, eventId, event) {
    var root = this;
    if (!root.hasEvent(helper, eventId)) {
        return false;
    }
    root.helpers[helper].eventHandlers[eventId].forEach(function (callback, index) {
        callback(event, index);
    });
};
window.$GnomeWorker = new GNOMEWORKER();
