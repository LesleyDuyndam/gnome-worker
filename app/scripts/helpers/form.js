'use strict';

var Form = function (externalSettings) {
    var root = this;
    root.errors = {};
    root.settings = {
        $element: $('form'),
        autoValidation: true,
        autoSubmit: true,
        updateErrorEvent: 'change',
        updateSuccessEvent: 'keyup',
        regex: {
            email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            zipcode: /^[1-9][0-9]{3}[ ]?([A-RT-Za-rt-z][A-Za-z]|[sS][BCbcE-Re-rT-Zt-z])$/
        },
        messages: {
            'required': 'Dit veld is verplicht',
            'required:email': 'Een email adres is verplicht!',
            'email': 'Dit is geen geldig email adres.',
            'zipcode': 'Voer postcode in als 1234AB.'
        }
    };
    if (externalSettings !== undefined) {
        root.settings = $.extend(root.settings, externalSettings);
    }
    root = $.extend(this, root.settings);
    root.listen();
    // return root;
};
Form.prototype.listen = function () {
    var root = this;
    if (root.autoValidation) {
        root.settings.$element.find('input, textarea').on('keyup change blur', root.handleInputEvent.bind(root));
    }
    if (root.autoSubmit) {
        root.settings.$element.on('submit', root.handleSubmitEvent.bind(root));
    }
};
Form.prototype.handleInputEvent = function (event) {
    var root = this;
    root.validateInput($(event.target), event.type);
};
Form.prototype.handleSubmitEvent = function (event) {
    var root = this;
    if (!root.validateForm($(event.target))) {
        return false;
    }
    // root.submitForm(event.target);
    event.preventDefault();
    return false;
};
Form.prototype.validateForm = function ($form) {
    var root = this;
    $form.find('input, textarea').each(function () {
        root.validateInput($(arguments[1]), 'change');
    });
    root.printErrors($form);
    return false;
};
Form.prototype.printErrors = function ($form) {
    $form.find('input, textarea').each(function (index, element) {
        var data = $(element).data();
        $(element).siblings('.error-message').remove();
        Object.keys(data).forEach(function (key) {
            $('<div class="error-message">' + $(element).data(key) + '</div>').insertAfter($(element));
        });
    });
    return false;
};
Form.prototype.validateInput = function ($input, type) {
    var root = this;
    if ($input.prop('required')) {
        root.validateRequired($input, type);
    }
    if ($input.attr('type') === 'email') {
        root.validateEmail($input, type);
    }
    if ($input.attr('name') === 'zipcode') {
        root.validateZipcode($input, type);
    }
    return true;
};
Form.prototype.validateRequired = function ($input, type) {
    var root = this;
    if ($input.attr('type') === 'checkbox' && !$input.prop('selected')) {
        root.addMessage('change', $input, 'required');
    } else {
        Form.removeMessage('change', $input, 'required');
    }
    if ($input.attr('type') !== 'checkbox' && $input.val() === '') {
        root.addMessage(type, $input, 'required');
    } else{
        Form.removeMessage(type, $input, 'required');
    }
};
Form.prototype.validateEmail = function ($input, type) {
    if ($input.val() === '') {
        return false;
    }
    var root = this;
    if (!root.regex.email.test($input.val())) {
        root.addMessage(type, $input, 'email');
    } else {
        Form.removeMessage(type, $input, 'email');
    }
};
Form.prototype.validateZipcode = function ($input, type) {
    if ($input.val() === '') {
        return false;
    }
    var root = this;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    console.dir(re.test($input.val()));
    if (!root.regex.zipcode.test($input.val())) {
        root.addMessage(type, $input, 'zipcode');
    } else {
        Form.removeMessage(type, $input, 'zipcode');
    }
};
Form.prototype.addMessage = function (type, $input, errorId) {
    var root = this;
    console.log(type);
    if (type === 'keyup') {
        return false;
    }
    var message = root.messages[errorId];
    if (root.messages[errorId + ':' + $input.attr('name')] !== undefined) {
        message = root.messages[errorId + ':' + $input.attr('name')];
    }
    $input.attr('data-validation-error-' + errorId, message);
};
Form.prototype.submit = function (event) {
    var root = this,
        $form = $(event.target);
    $.ajax({
        method: $form.attr('method') || 'post',
        url: $form.attr('action') || 'api/formHandler',
        success: root.submitSuccessHandler,
        error: root.submitErrorHandler
    });
};
Form.prototype.submitSuccessHandler = function (event) {
    if ($GnomeWorker !== undefined) {
        $GnomeWorker.broadcast('Form', 'success', event);
    }
};
Form.prototype.submitErrorHandler = function (error) {
    if ($GnomeWorker !== undefined) {
        $GnomeWorker.broadcast('Form', 'error', error);
    }
};
Form.removeMessage = function (type, $input, errorId) {
    $input.attr('data-validation-error-' + errorId, false);
};
